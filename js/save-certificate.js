function validateForm() {
    let itemName = document.getElementById('certificate-name').value.trim();
    let images = document.getElementById('certificate-image').files;
    let price = document.getElementById('certificate-price').value.trim();
    let validFor = document.getElementById('certificate-validity').value.trim();
    let description = document.getElementById('certificate-description').value.trim();
    let tags = document.getElementById('tag-list').children;

    if (!itemName) {
        alert('Certificate Name field is empty');
        return false;
    }

    if (images.length === 0) {
        // alert('No image was chosen');
        // return false;
    }

    if (!price || isNaN(price) || price <= 0) {
        alert('Price field is empty or contains incorrect data');
        return false;
    }

    if (!validFor || isNaN(validFor) || validFor < 1) {
        alert('Valid for field is empty, contains non-numeric data or less than 1');
        return false;
    }

    if (!description) {
        alert('Description field is empty');
        return false;
    }

    if (tags.length === 0) {
        alert('At least one tag should be added');
        return false;
    }

    return true;
}

document.querySelector('.save-button').addEventListener('click', function (event) {
    event.preventDefault();

    if (validateForm()) {
        addCertificate();
    }
});

function addCertificate() {
    // Capture the data entered by the user
    const certificateName = document.getElementById('certificate-name').value;
    const certificatePrice = document.getElementById('certificate-price').value;
    const certificateValidity = document.getElementById('certificate-validity').value;
    const certificateDescription = document.getElementById('certificate-description').value;
    const certificateTags = Array.from(document.getElementById('tag-list').children).map(tag => tag.textContent);
    const images = document.getElementById('certificate-image').files;
    let certificateImage = "coupon.png";
    if (images.length !== 0) {
        certificateImage = document.getElementById('certificate-image').files[0].name;  // get the file name
    }

    // Create a certificate object
    const certificate = {
        name: certificateName,
        price: certificatePrice,
        validFor: certificateValidity,
        description: certificateDescription,
        image: certificateImage,
        tags: certificateTags,
        date: new Date().toISOString().split('T')[0]  // Get current date in 'YYYY-MM-DD' format
    };

    // Save the certificate
    saveCertificate(certificate);
}

function saveCertificate(certificate) {
    // Get the existing certificates from local storage
    let certificates = localStorage.getItem('certificates');

    // Parse the JSON string to an array
    certificates = JSON.parse(certificates);

    // If there are no certificates in local storage, create a new array
    if (!certificates) {
        certificates = [];
    }

    // Add the new certificate to the array
    certificates.push(certificate);

    // Save the certificates array back to local storage
    localStorage.setItem('certificates', JSON.stringify(certificates));
}

// Select the tag input field
const tagInput = document.getElementById('tag-input');
const addTagButton = document.getElementById('add-tag-button');

// Function to create and add a tag
function createTag() {
    // Get the input value
    const tagValue = tagInput.value.trim();

    // If input value is not empty
    if (tagValue) {
        // Create new label
        const newTag = document.createElement('label');
        newTag.textContent = tagValue;
        newTag.className = 'tag';

        // create delete button
        let deleteButton = document.createElement('span');
        deleteButton.textContent = 'x';
        deleteButton.className = 'delete-tag';
        newTag.appendChild(deleteButton);

        // Append the new tag to the tag list
        document.getElementById('tag-list').appendChild(newTag);

        // Clear the input field
        tagInput.value = '';
    }
}

document.getElementById('tag-list').addEventListener('click', function(e) {
    // Check if the clicked element is a delete button
    if (e.target && e.target.classList.contains('delete-tag')) {
        // remove the parent node (tag)
        e.target.parentNode.remove();
    }
});

// Attach a keyup event listener to tag input field
tagInput.addEventListener('keyup', function (event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
        // Prevent the form from being submitted
        event.preventDefault();

        // Create a new tag
        createTag();
    }
});

addTagButton.addEventListener('click', function (event) {
    // Prevent form from submitting
    event.preventDefault();

    // Create a new tag
    createTag();
});
