let currentPage = 0;
let fetching = false;
const imgDir = "img/";
const certificatesPerPage = 6;
let searchText = '';
let categoriesCount = {};
let sortMethod = 'date-desc';
let sortSwitcher = document.querySelector('#sort-options');

window.addEventListener('scroll', _.throttle(handleScroll, 200));

sortSwitcher.addEventListener('change', function (event) {
    sortMethod = event.target.value;
    currentPage = 0;
    fetching = false;
    let container = document.getElementById('certificate-section');
    container.innerHTML = '';
    fetchMoreCertificates();
});


function addCertificateToPage(certificate) {

    let container = document.getElementById('certificate-section');

    let certificateElement = document.createElement('div');
    certificateElement.classList.add('certificate-tile');

    certificateElement.innerHTML = `
        <img src="${imgDir}${certificate.image}" alt="Image of ${certificate.name}">
        
        <div class="certificate-info">
            <div class="certificate-details">
                <h3 class="certificate-name">${certificate.name}</h3>
                <p>Description: ${certificate.description}</p>
            </div>
            <div class="certificate-meta column-section">
                <span class="material-symbols-outlined favorites-icon icon">favorite</span>
            </div>
        </div>
        <div class="certificate-action">
        <p class="certificate-action">Price: ${certificate.price}</p>
        <button class="add-to-cart ok-button">Add to Cart</button>
    `;

    container.appendChild(certificateElement);
}

function defineCertificates() {
    // Get the certificates from local storage
    let certificates = JSON.parse(localStorage.getItem('certificates') || '[]');

    let savedPage = localStorage.getItem('currentPage');
    if (savedPage) {
        currentPage = parseInt(savedPage);
        localStorage.removeItem('currentPage');
    }

    for (let i = 0; i <= currentPage; i++) {
        let pageCertificates = certificates.slice(i * certificatesPerPage, (i + 1) * certificatesPerPage);
        pageCertificates.forEach(certificate => {
            addCertificateToPage(certificate);
        });
    }

    currentPage++;
}

function handleScroll() {
    // Check if we're close to the bottom of the page
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 500) {
        // We're close to the bottom of the page, so fetch more certificates
        fetchMoreCertificates();
    }
}

function sortCertificates(certificates) {
    switch (sortMethod) {
        case 'price-asc':
            return certificates.sort((a, b) => a.price - b.price);
        case 'price-desc':
            return certificates.sort((a, b) => b.price - a.price);
        case 'name-asc':
            return certificates.sort((a, b) => a.name.localeCompare(b.name));
        case 'name-desc':
            return certificates.sort((a, b) => b.name.localeCompare(a.name));
        case 'date-asc':
            return certificates.sort((a, b) => new Date(a.date) - new Date(b.date));
        case 'date-desc':
            return certificates.sort((a, b) => new Date(b.date) - new Date(a.date));
        default:
            return certificates;
    }
}

function fetchMoreCertificates() {
    if (fetching) {
        return;
    }

    fetching = true;

    let certificates = JSON.parse(localStorage.getItem('certificates') || '[]');

    // Sort the certificates according to the selected sort method
    certificates = sortCertificates(certificates);

    // Create a slice of certificates starting from the current page
    let nextPage = certificates.slice(currentPage * certificatesPerPage);

    // Filter the nextPage array and only include certificates that pass the search
    nextPage = nextPage.filter(certificate => {
        return certificate.name.toLowerCase().includes(searchText) ||
            certificate.description.toLowerCase().includes(searchText) ||
            certificate.tags.join(',').toLowerCase().includes(searchText);
    });

    // Only take the first 'certificatesPerPage' items from nextPage
    nextPage = nextPage.slice(0, certificatesPerPage);

    // If there are no more certificates, stop fetching
    if (!nextPage.length) {
        fetching = false;
        return;
    }

    nextPage.forEach(certificate => {
        addCertificateToPage(certificate);
    });

    currentPage++;
    fetching = false;
}

function resetPage() {
    currentPage = 0;
    fetching = false;
    let container = document.getElementById('certificate-section');
    container.innerHTML = '';
}

function handleSearchInput(event) {
    searchText = event.target.value.toLowerCase();
    resetPage();
    fetchMoreCertificates();
}

function attachSearchHandler() {
    let searchInput = document.querySelector('.search-container input[name="search"]');
    let debouncedHandler = _.debounce(handleSearchInput, 300);
    searchInput.addEventListener('input', debouncedHandler);
}

function definePopularCategories() {

    let certificates = JSON.parse(localStorage.getItem('certificates') || '[]');

    certificates.forEach(certificate => {
        certificate.tags.forEach(tag => {
            if (!categoriesCount[tag]) {
                categoriesCount[tag] = 1;
            } else {
                categoriesCount[tag]++;
            }
        });
    });

    let popularCategories = Object.entries(categoriesCount)
        .map(([category, count]) => ({category, count}))
        .sort((a, b) => b.count - a.count)
        .slice(0, 7);

    let allCategoriesElement = document.createElement('div');
    allCategoriesElement.classList.add("category-tile")
    allCategoriesElement.classList.add("column-section")
    allCategoriesElement.innerHTML=`
            <img src="img/travel.png" alt="Category Image">
            <p class="category-name">All</p>
        `;

    allCategoriesElement.addEventListener('click', function () {
        searchText = '';
        currentPage = 0;
        fetching = false;
        let container = document.getElementById('certificate-section');
        container.innerHTML = '';
        fetchMoreCertificates();
    });

    // Append the all categories element to your popular categories container
    document.getElementById('popular-categories').appendChild(allCategoriesElement);


    popularCategories.forEach(categoryObj => {
        let categoryElement = document.createElement('div');
        categoryElement.classList.add("category-tile")
        categoryElement.classList.add("column-section")
        categoryElement.innerHTML=`
            <img src="img/travel.png" alt="Category Image">
            <p class="category-name">${categoryObj.category}</p>
        `;

        categoryElement.addEventListener('click', function () {
            searchText = categoryObj.category.toLowerCase();
            currentPage = 0;
            fetching = false;
            let container = document.getElementById('certificate-section');
            container.innerHTML = '';
            fetchMoreCertificates();
        });

        document.getElementById('popular-categories').appendChild(categoryElement);
    });
}

window.onload = function () {
    defineCertificates();
    definePopularCategories();
    attachSearchHandler();
}


