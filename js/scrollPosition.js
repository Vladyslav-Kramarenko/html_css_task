let scrollReturnButton = document.getElementById("scrollReturn");
let backToTopButton = document.getElementById("backToTop");

window.onscroll = function () {
    scrollFunction()
};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        scrollReturnButton.style.display = "none";
        backToTopButton.style.display = "block";
    } else {
        scrollReturnButton.style.display = "block";
        backToTopButton.style.display = "none";
    }
}

window.addEventListener('beforeunload', function () {
    localStorage.setItem('currentPage', currentPage);
});


function returnScroll() {
    const scrollPosition = localStorage.getItem('scrollPosition');
    window.scrollTo(0, scrollPosition);
    localStorage.removeItem('scrollPosition');
}

function topFunction() {
    localStorage.setItem('scrollPosition', window.scrollY);
    console.log("scrollPosition = " + window.scrollY)
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}